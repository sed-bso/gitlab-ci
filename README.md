# gitlab-ci

This repository hosts a presentation of the gitlab-ci service to
perform continuous integration of git projects hosted on GitLab.

Please refer to this
[website](https://sed-bso.gitlabpages.inria.fr/gitlab-ci/) to read the
content of the presentation.
